# Les entiers signés





## Histoire drôle : vol 501 d'Ariane V

Le 4 juin 1996,  la fusée Ariane V a été détruite en  vol, 37 secondes après son
lancement, suite à un problème dans la centrale inertielle.

Déjà  utilisé sur  Ariane IV,  le système  a eu  un comportement  inattendu avec
Ariane V.

Initialement  codée comme  nombre flottant  double précision  (sur 64  bits), la
valeur  de l'accélération  horizontale était  convertie durant  le processus  de
contrôle de la fusée, en un nombre entier signé, codé sur 16 bits.

La valeur de l'accélération étant trop grande, la conversion du nombre en entier
a échoué,  provoquant une réaction  en chaîne dans  le pilote automatique  qui a
abouti à l'autodestruction de la fusée.

Comment l'expliquer ? Et comment expliquer qu'un 



## Convention

Les entiers naturels sont codés sur machine en base 2 sur un nombre
arbitraire de bits.

*Pour simplifier  nos illustrations, nous  considérerons des entiers codés  sur 8
bits.*


On  peut travailler  sur 8  bits en  Python  grâce à  la fonction  `int8` de  la
bibliothèque `numpy`.

```python
# On importe la bibliothèque numpy
import numpy

# On définit une fonction qui codera les entiers sur 8 bits
def i8(n):
    return numpy.int8(n)
```

Quel est le plus grand nombre entier représentable sur 8 bits ?

On observe de drôles de choses...

```python
In [1]: [i8(n) for n in range(123,132)]
Out[1]: [123, 124, 125, 126, 127, -128, -127, -126, -125]
```

Comment l'expliquer ????


## Le bit de signe


Définition :

> Le bit de poids fort (le plus à gauche) est réservé pour le signe: 1 pour les négatifs, 0 pour les positifs.


Remarque :

> Mais cela pose des problèmes si on se contente de ce codage.  Par exemple, sur
> quatre bits, $`3 + (-2)`$ serait égal à $`-5`$

```
       0 0 1 1       -->  3
     + 1 0 1 0       --> -2
     ---------
       1 1 0 1       --> -5
``` 




## Le complément à $`2^n`$

### Le complément à 1


Définition :

>Prendre le complément à 1 d'un nombre consiste à inverser les 0 et les 1 de son
>écriture binaire.

Par exemple, le complément à 1 de `1001` est `0110`.


### L'opposé d'un nombre


Définition :

>L'opposé d'un nombre $`x`$ est le nombre $`y`$ vérifiant $`x+y=0`$.

Par exemple, l'opposé de 3 est $`-3`$ car $`3+(-3) = 0`$



### Opposé d'un nombre en 8 bits

* On  remarque astucieusement qu'un  nombre d'un octet  plus son complément  à 1
  s'écrit `1111 1111`. 
  Par exemple sur 8 bits `1001 1010 + 0110 0101 = 1111 1111`.
*  Par la  suite  on  notera $`\bar{b}`$  le  complément à  1  du  bit $`b`$.  Ainsi
  $`\bar{1}=0`$ et $`\bar{0}=1`$.
* Alors $`(b_7b_6b_5b_4b_3b_2b_1b_0 + \bar{b_7}\bar{b_6}\bar{b_5}\bar{b_4}\bar{b_3}\bar{b_2}\bar{b_1}\bar{b_0}) +1 =  1111\,1111 + 1 = (1)0000\,0000`$
* donc  $`b_7b_6b_5b_4b_3b_2b_1b_0 +   \bar{b_7}\bar{b_6}\bar{b_5}\bar{b_4}\bar{b_3}\bar{b_2}\bar{b_1}\bar{b_0} + 1 = 0`$
 
 
Définition :

> Par convention:
> *L'opposé d'un nombre positif est son complément à 1 plus 1.\  
> *L'opposé d'un nombre négatif est 1 moins son complément à 1.

## Exemples

### Comment s'écrit (-3) sur 8 bits?

* On cherche l'opposé du nombre positif 3.
* 3  s'écrit `0000 0011` sur 8 bits.
* Le complément à 1 est donc `1111 1100` 
* L'opposé $(-3)$ s'écrit alors `1111 1100 + 1 = 1111 1101`

### À quel entier relatif correspond 1011 0101 ?

* C'est un nombre négatif car son premier bit vaut 1.
* On lui retranche 1: `1011 0101 -1 = 1011 0100`.
* Il ne reste plus qu'à prendre le complément à 1 de ce dernier nombre: 
`0100 1011`. Il s'agit donc de $`2^0 + 2^1 + 2^3+2^6=1+2+8+64=75`$ 

`1011 0101` est donc l'écriture sur 8 bits de $`-75`$.






## Exercices


### Exercice 1 - Ariane

Expliquer ce qui s'est passé lors de l'accident de la fusée Ariane V.

### Exercice 2 - Codage en complément à $`2^{16}`$

Dans cet exercice, on utilisera le codage en complément à 2 sur 16 chiffres binaires (2 octets).
Quels sont les nombres minimum et maximum que l'on peut représenter ?

Donnez le codage des nombres suivants :

| base 10 | Codage en complément à 2 |
| ------- | ------------------------ |
| -513    |                          |
| 512     |                          |
| -1      |                          |
| 0       |                          |
| -32768  |                          |
|         |        0000000010000000  |
|         |        0111111111111111  |
|         |        1000000000000001  |



### Exercice 3 - Additions


Le  codage  en complément  à  $`2^n`$  est  utilisé  car il  permet  d'effectuer
simplement les  additions : si  on additionne  2 nombres (positifs  ou négatifs)
codés en complément  à $`2^n`$ sur 8  bits, le résultat (s'il est  dans la plage
des nombres  représentables) est aussi  un nombre codé en  complément à 2  sur 8
bits.

1. Convertissez les 2 nombres suivants en complément à $`2^n`$ sur 8 bits : 12, -53
2. Effectuez l'addition en binaire des deux nombres
3. Vérifiez que le résultat est correct


### Exercice 4 - Additions étranges
Expliquez pourquoi  sur 8 bits  on obtient  les résultats suivants  présentés en
cours:


~~~~ {#python_relatifs1_rep .python .numberLines}
127 - 1 = 126
127 + 1 = -128
127 + 2 = -127
127 + 127 = -2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




### Exercice 5 - Le Bug de l'an 2038 

Les machines  UNIX suivent la norme  IEEE 1003 (ou norme  POSIX) qui spécifie,
entre autre, que le temps est compté en  secondes à partir du 1er janvier 1970 à
00:00:00 Temps Universel.  De nombreux systèmes de fichiers codent  ce temps en un
entier signé 32 bits (le signe permet de désigner des dates antérieures à 1970).

Pourquoi parle-t-on alors du bug de l'an 2038? 




### Exercice 6 - Python

Créez une fonction  Python qui renvoie l'écriture d'un nombre  entier en binaire
   signé sur 16 bits sous forme de chaîne de caractères.
   
   Créez une fonction qui effectue l'opération inverse.







